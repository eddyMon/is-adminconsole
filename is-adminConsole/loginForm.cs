﻿using is_adminConsole.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace is_adminConsole
{
    public partial class loginForm : Form
    {
        public static string loginEmail = "";
        public static string username = "";
        public static string GATEWAY_IP = "https://localhost:44384/";

        public loginForm()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            //send creds to the API and return email and username at least
            //if(txtEmail.Text.Equals("admin@mail.com") && txtPass.Text.Equals("pw"))
            //blocked users can't log in????

            if (txtEmail.TextLength == 0)
            {
                MessageBox.Show("Please insert the  you want to add");
                return;
            }

            //fetch user with given email API
            var gatewayClient = new RestClient(GATEWAY_IP);
            byte[] emailBytes = Encoding.ASCII.GetBytes(txtEmail.Text);
            string emailBase64 = System.Convert.ToBase64String(emailBytes);
            //MessageBox.Show(emailBase64);
            var request = new RestRequest("api/admins/credentials/" + emailBase64, Method.GET);
            var qryResult = gatewayClient.Execute<Admin>(request);
            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                //MessageBox.Show("New category -> " + qryResult.Content);
                Admin admin = qryResult.Data;
                string authHeader = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(admin.Id+ ':' + txtPass.Text));
                var loginRequest = new RestRequest("api/admins/login", Method.POST);
                request.AddHeader("Authorization", "Basic " + authHeader.ToString()); ;
                var loginQryResult = gatewayClient.Execute<Admin>(request);

                //login request
                if(loginQryResult.StatusCode == HttpStatusCode.OK)
                {
                    this.Hide();
                    loginEmail = txtEmail.Text;
                    //get admin name from the APi
                    username = "Admin 1";
                    var adminConsole = new Form1(admin);
                    adminConsole.Closed += (s, args) => this.Close();
                    adminConsole.Show();
                }
                else
                {
                    MessageBox.Show("Incorrect credentials were provided");
                }
            }
            else
            {
                MessageBox.Show("No administrators were found with the given credentials -> "+qryResult.StatusCode.ToString());
            }
        }
    }
}
