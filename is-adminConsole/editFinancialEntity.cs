﻿using is_adminConsole.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace is_adminConsole
{
    public partial class editFinancialEntity : Form
    {
        Form1 form;
        int selectedIndex;
        BankListItem selectedElement;
        public editFinancialEntity(Form1 form, int selectedIndex, BankListItem selectedElement)
        {
            InitializeComponent();
            this.form = form;
            this.selectedIndex = selectedIndex;
            this.selectedElement = selectedElement;
        }

        private void btnEditFinancial_Click(object sender, EventArgs e)
        {
            if (txtFinancialEndpoint.Text.Length == 0 && txtFinancialEntityName.Text.Length == 0)
            {
                MessageBox.Show("You need to fill at least one field! [Endpoint,Name]");
                return;
            }

            //call API
            String GATEWAY_IP = Form1.GATEWAY_IP;
            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/banks/"+ selectedElement.Bank.bankCode, Method.PATCH);
            request.RequestFormat = DataFormat.Json;

            if(txtFinancialEndpoint.Text.Length > 0 && txtFinancialEntityName.Text.Length > 0)
            {
                request.AddJsonBody(new
                {
                    //send both the endpoint and the name
                    name = txtFinancialEntityName.Text,
                    endpoint = txtFinancialEndpoint.Text
                });
            }
            else
            {
                if(txtFinancialEndpoint.Text.Length > 0)
                {
                    //send only the endpoint
                    request.AddJsonBody(new
                    {
                        endpoint = txtFinancialEndpoint.Text
                    });
                }
                else
                {
                    //send only the name
                    request.AddJsonBody(new
                    {
                        name = txtFinancialEntityName.Text
                    });
                }
            }
            

            var qryResult = gatewayClient.Execute<Bank>(request);

            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                this.form.RefreshBankList(qryResult.Data, selectedIndex, selectedElement.status);
                this.Close();
            }
            else
            {
                MessageBox.Show("There was an error updating the Financial Entity -> " + qryResult.ErrorMessage);
            }
        }
    }
}
