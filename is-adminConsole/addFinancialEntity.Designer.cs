﻿
namespace is_adminConsole
{
    partial class addFinancialEntity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFinancialName = new System.Windows.Forms.TextBox();
            this.txtFinancialEndpoint = new System.Windows.Forms.TextBox();
            this.btnAddFinancial = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFinancialBankCode = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(70, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = "Add Financial Entity";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(29, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 20);
            this.label9.TabIndex = 32;
            this.label9.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 20);
            this.label2.TabIndex = 33;
            this.label2.Text = "Endpoint:";
            // 
            // txtFinancialName
            // 
            this.txtFinancialName.Location = new System.Drawing.Point(123, 77);
            this.txtFinancialName.Name = "txtFinancialName";
            this.txtFinancialName.Size = new System.Drawing.Size(219, 20);
            this.txtFinancialName.TabIndex = 34;
            // 
            // txtFinancialEndpoint
            // 
            this.txtFinancialEndpoint.Location = new System.Drawing.Point(123, 139);
            this.txtFinancialEndpoint.Name = "txtFinancialEndpoint";
            this.txtFinancialEndpoint.Size = new System.Drawing.Size(219, 20);
            this.txtFinancialEndpoint.TabIndex = 35;
            // 
            // btnAddFinancial
            // 
            this.btnAddFinancial.Location = new System.Drawing.Point(159, 177);
            this.btnAddFinancial.Name = "btnAddFinancial";
            this.btnAddFinancial.Size = new System.Drawing.Size(75, 23);
            this.btnAddFinancial.TabIndex = 37;
            this.btnAddFinancial.Text = "Create";
            this.btnAddFinancial.UseVisualStyleBackColor = true;
            this.btnAddFinancial.Click += new System.EventHandler(this.btnAddFinancial_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 20);
            this.label3.TabIndex = 38;
            this.label3.Text = "Bank code:";
            // 
            // txtFinancialBankCode
            // 
            this.txtFinancialBankCode.Location = new System.Drawing.Point(123, 108);
            this.txtFinancialBankCode.Name = "txtFinancialBankCode";
            this.txtFinancialBankCode.Size = new System.Drawing.Size(219, 20);
            this.txtFinancialBankCode.TabIndex = 39;
            // 
            // addFinancialEntity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 216);
            this.Controls.Add(this.txtFinancialBankCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnAddFinancial);
            this.Controls.Add(this.txtFinancialEndpoint);
            this.Controls.Add(this.txtFinancialName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "addFinancialEntity";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "addFinancialEntity";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFinancialName;
        private System.Windows.Forms.TextBox txtFinancialEndpoint;
        private System.Windows.Forms.Button btnAddFinancial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFinancialBankCode;
    }
}