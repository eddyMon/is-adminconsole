﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole
{
    using Excel = Microsoft.Office.Interop.Excel;
    class ExcelHandler
    {


        public static void CreateNewExcelFile(string filename)
        {
            //Creates an Excel Application instance
            var excelAplication = new Excel.Application();
            excelAplication.Visible = true;

            //Creates and Excel Workbook with a default number of sheets
            var excelWorkbook = excelAplication.Workbooks.Add();
            excelWorkbook.SaveAs(filename, AccessMode: Excel.XlSaveAsAccessMode.xlNoChange);

            excelWorkbook.Close();
            excelAplication.Quit();

            ReleaseCOMObjects(excelAplication);
            ReleaseCOMObjects(excelWorkbook);




        }

        public static void ReleaseCOMObjects(Object objectToBeReleased)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objectToBeReleased);
                objectToBeReleased = null;
                GC.Collect();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception caught: " + e);
            }

        }

        public static void WriteToExcelFile(string filename, string[,] transMatrix)
        {
            Excel.Application excelApplication = new Excel.Application();
            excelApplication.Visible = true;

            //Open the excel file
            var excelWorkbook = excelApplication.Workbooks.Open(filename);
            var excelWorksheet = (Excel.Worksheet)excelWorkbook.ActiveSheet;


            for(int i=0; i< transMatrix.Length; i++)
            {
                for(int j=0; j<transMatrix[i,j].Length; j++)
                {
                    excelWorksheet.Cells[i + 1, j + 1].Value = transMatrix[i, j];
                }
            }

            //excelWorksheet.Cells[1, 1].Value = "Hello";
            //excelWorksheet.Cells[1, 2].Value = "World!";

            excelWorkbook.Save();
            excelWorkbook.Close();
            excelApplication.Quit();

            //Don't forget to free the memory used by the excel objects
            //....

            ReleaseCOMObjects(excelWorkbook);
            ReleaseCOMObjects(excelApplication);
        }

        public static string ReadFromExcelFile(string filename)
        {
            var excelApplication = new Excel.Application();
            excelApplication.Visible = false;

            //Open the excel file
            var excelWorkbook = excelApplication.Workbooks.Open(filename);
            var excelWorksheet = (Excel.Worksheet)excelWorkbook.ActiveSheet;

            string content = excelWorksheet.Cells[1, 1].Value;
            content += (excelWorksheet.Cells[1, 2] as Excel.Range).Text;

            excelWorkbook.Close();
            excelApplication.Quit();

            //Don't forget to free the memory used by the excel objects
            //....

            //release memmory from COM Objects
            ReleaseCOMObjects(excelWorkbook);
            ReleaseCOMObjects(excelApplication);

            return content;
        }

        public static void createChart(string filename)
        {
            //creates an Excel application instance
            var excelApplication = new Excel.Application();
            excelApplication.Visible = true;

            //creates an Exel Workbook with a default number of sheets
            var excelWorkbook = excelApplication.Workbooks.Add();
            Excel.Worksheet excelWorksheet = excelWorkbook.ActiveSheet;


            //Add a chart object
            Excel.Chart myChart = null;
            Excel.ChartObjects charts = excelWorksheet.ChartObjects();
            Excel.ChartObject chartObj = charts.Add(50, 50, 300, 300);//Left; Top; Widthh; Height
            myChart = chartObj.Chart;

            //set chart range -- cell values to be used in the graph
            Excel.Range myRange = excelWorksheet.get_Range("B1:B4");
            myChart.SetSourceData(myRange);

            //chart propertyies using the named properties and default parameters functionality in
            //the .NET Framework
            myChart.ChartType = Excel.XlChartType.xlLine;
            myChart.ChartWizard(Source: myRange,
                Title: "Graph Title",
                CategoryTitle: "Title of X axis...",
                ValueTitle: "Title of y axis...");

            excelWorkbook.SaveAs(filename);
            excelWorkbook.Close();
            excelApplication.Quit();

            ReleaseCOMObjects(excelWorkbook);
            ReleaseCOMObjects(excelApplication);
        }

        public static string ReadNMFromExcelFile(string filename, string n, string m)
        {
            var excelApplication = new Excel.Application();
            excelApplication.Visible = false;

            //Open the excel file
            var excelWorkbook = excelApplication.Workbooks.Open(filename);
            var excelWorksheet = (Excel.Worksheet)excelWorkbook.ActiveSheet;

            //NxM A1:B4
            var range = excelWorksheet.get_Range(n, m);
            string content = "";

            foreach (Excel.Range item in range)
            {
                content += item.Value + " ";
            }

            excelWorkbook.Close();
            excelApplication.Quit();

            //Don't forget to free the memory used by the excel objects
            //....

            //release memmory from COM Objects
            ReleaseCOMObjects(excelWorkbook);
            ReleaseCOMObjects(excelApplication);

            return content;
        }

        public static string ReadFromExcelWorksheet(string filename, string worksheetName)
        {
            var excelApplication = new Excel.Application();
            excelApplication.Visible = false;

            //Open the excel file
            var excelWorkbook = excelApplication.Workbooks.Open(filename);
            //var excelWorksheet = (Excel.Worksheet)excelWorkbook.Worksheets.get_Item(worksheet);
            //buscar worksheet pelo nome
            var excelWorksheet = (Excel.Worksheet)excelWorkbook.Worksheets[worksheetName];

            string content = "";
            /*
            for(int i = 1; i < excelWorksheet.Rows.Count;)
            {
                for(int j = 1; i < excelWorksheet.Columns.Count;)
                {
                    content += excelWorksheet.Cells[i, j].Text;
                }
            }
            */
            content += excelWorksheet.Cells[1, 1].Text;



            excelWorkbook.Close();
            excelApplication.Quit();

            //Don't forget to free the memory used by the excel objects
            //....

            //release memmory from COM Objects
            ReleaseCOMObjects(excelWorkbook);
            ReleaseCOMObjects(excelApplication);

            return content;
        }
    }
}
