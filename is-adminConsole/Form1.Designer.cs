﻿
namespace is_adminConsole
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdateStatus = new System.Windows.Forms.Button();
            this.btnAddFinancialEntity = new System.Windows.Forms.Button();
            this.btnEditFinancialEntity = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRemoveFinancialEntity = new System.Windows.Forms.Button();
            this.btnRemoveDebitCategory = new System.Windows.Forms.Button();
            this.btnAddDebitCategory = new System.Windows.Forms.Button();
            this.btnRemoveCreditCategory = new System.Windows.Forms.Button();
            this.btnAddCreditCategory = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnRemoveAdmin = new System.Windows.Forms.Button();
            this.btnAddAdmin = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnUnblockVcard = new System.Windows.Forms.Button();
            this.btnBlockVcard = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnExportXML = new System.Windows.Forms.Button();
            this.btnExportXLSX = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.btnUnblockAdmin = new System.Windows.Forms.Button();
            this.btnBlockAdmin = new System.Windows.Forms.Button();
            this.btnManageVcard = new System.Windows.Forms.Button();
            this.btnCreateVcard = new System.Windows.Forms.Button();
            this.lbTransactionLog = new System.Windows.Forms.ListBox();
            this.lbFinancialEntities = new System.Windows.Forms.ListBox();
            this.lbAdminList = new System.Windows.Forms.ListBox();
            this.lbDefaultCreditCat = new System.Windows.Forms.ListBox();
            this.lbDefaultDebitCat = new System.Windows.Forms.ListBox();
            this.lbVcardList = new System.Windows.Forms.ListBox();
            this.txtAddDebitCat = new System.Windows.Forms.TextBox();
            this.txtAddCreditCat = new System.Windows.Forms.TextBox();
            this.comboEntities = new System.Windows.Forms.ComboBox();
            this.chkCredit = new System.Windows.Forms.CheckBox();
            this.chkDebit = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtStartDate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEndDate = new System.Windows.Forms.TextBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(441, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(348, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "Administration Console";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(58, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Financial Entities";
            // 
            // btnUpdateStatus
            // 
            this.btnUpdateStatus.Location = new System.Drawing.Point(62, 266);
            this.btnUpdateStatus.Name = "btnUpdateStatus";
            this.btnUpdateStatus.Size = new System.Drawing.Size(145, 35);
            this.btnUpdateStatus.TabIndex = 3;
            this.btnUpdateStatus.Text = "Update status";
            this.btnUpdateStatus.UseVisualStyleBackColor = true;
            this.btnUpdateStatus.Click += new System.EventHandler(this.btnUpdateStatus_Click);
            // 
            // btnAddFinancialEntity
            // 
            this.btnAddFinancialEntity.Location = new System.Drawing.Point(62, 307);
            this.btnAddFinancialEntity.Name = "btnAddFinancialEntity";
            this.btnAddFinancialEntity.Size = new System.Drawing.Size(145, 35);
            this.btnAddFinancialEntity.TabIndex = 4;
            this.btnAddFinancialEntity.Text = "Add new financial entity";
            this.btnAddFinancialEntity.UseVisualStyleBackColor = true;
            this.btnAddFinancialEntity.Click += new System.EventHandler(this.btnAddFinancialEntity_Click);
            // 
            // btnEditFinancialEntity
            // 
            this.btnEditFinancialEntity.Location = new System.Drawing.Point(62, 348);
            this.btnEditFinancialEntity.Name = "btnEditFinancialEntity";
            this.btnEditFinancialEntity.Size = new System.Drawing.Size(145, 35);
            this.btnEditFinancialEntity.TabIndex = 5;
            this.btnEditFinancialEntity.Text = "Edit financial entity";
            this.btnEditFinancialEntity.UseVisualStyleBackColor = true;
            this.btnEditFinancialEntity.Click += new System.EventHandler(this.btnEditFinancialEntity_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(302, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "Default Debit Categories";
            // 
            // btnRemoveFinancialEntity
            // 
            this.btnRemoveFinancialEntity.Location = new System.Drawing.Point(62, 389);
            this.btnRemoveFinancialEntity.Name = "btnRemoveFinancialEntity";
            this.btnRemoveFinancialEntity.Size = new System.Drawing.Size(145, 35);
            this.btnRemoveFinancialEntity.TabIndex = 8;
            this.btnRemoveFinancialEntity.Text = "Remove financial entity";
            this.btnRemoveFinancialEntity.UseVisualStyleBackColor = true;
            this.btnRemoveFinancialEntity.Click += new System.EventHandler(this.btnRemoveFinancialEntity_Click);
            // 
            // btnRemoveDebitCategory
            // 
            this.btnRemoveDebitCategory.Location = new System.Drawing.Point(339, 415);
            this.btnRemoveDebitCategory.Name = "btnRemoveDebitCategory";
            this.btnRemoveDebitCategory.Size = new System.Drawing.Size(145, 35);
            this.btnRemoveDebitCategory.TabIndex = 11;
            this.btnRemoveDebitCategory.Text = "Remove category";
            this.btnRemoveDebitCategory.UseVisualStyleBackColor = true;
            this.btnRemoveDebitCategory.Click += new System.EventHandler(this.btnRemoveDebitCategory_Click);
            // 
            // btnAddDebitCategory
            // 
            this.btnAddDebitCategory.Location = new System.Drawing.Point(339, 374);
            this.btnAddDebitCategory.Name = "btnAddDebitCategory";
            this.btnAddDebitCategory.Size = new System.Drawing.Size(145, 35);
            this.btnAddDebitCategory.TabIndex = 10;
            this.btnAddDebitCategory.Text = "Add category";
            this.btnAddDebitCategory.UseVisualStyleBackColor = true;
            this.btnAddDebitCategory.Click += new System.EventHandler(this.btnAddDebitCategory_Click);
            // 
            // btnRemoveCreditCategory
            // 
            this.btnRemoveCreditCategory.Location = new System.Drawing.Point(574, 415);
            this.btnRemoveCreditCategory.Name = "btnRemoveCreditCategory";
            this.btnRemoveCreditCategory.Size = new System.Drawing.Size(145, 35);
            this.btnRemoveCreditCategory.TabIndex = 15;
            this.btnRemoveCreditCategory.Text = "Remove category";
            this.btnRemoveCreditCategory.UseVisualStyleBackColor = true;
            this.btnRemoveCreditCategory.Click += new System.EventHandler(this.btnRemoveCreditCategory_Click);
            // 
            // btnAddCreditCategory
            // 
            this.btnAddCreditCategory.Location = new System.Drawing.Point(574, 374);
            this.btnAddCreditCategory.Name = "btnAddCreditCategory";
            this.btnAddCreditCategory.Size = new System.Drawing.Size(145, 35);
            this.btnAddCreditCategory.TabIndex = 14;
            this.btnAddCreditCategory.Text = "Add category";
            this.btnAddCreditCategory.UseVisualStyleBackColor = true;
            this.btnAddCreditCategory.Click += new System.EventHandler(this.btnAddCreditCategory_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(537, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(216, 24);
            this.label4.TabIndex = 12;
            this.label4.Text = "Default Credit Categories";
            // 
            // btnRemoveAdmin
            // 
            this.btnRemoveAdmin.Location = new System.Drawing.Point(1055, 389);
            this.btnRemoveAdmin.Name = "btnRemoveAdmin";
            this.btnRemoveAdmin.Size = new System.Drawing.Size(145, 35);
            this.btnRemoveAdmin.TabIndex = 20;
            this.btnRemoveAdmin.Text = "Remove administrator";
            this.btnRemoveAdmin.UseVisualStyleBackColor = true;
            this.btnRemoveAdmin.Click += new System.EventHandler(this.btnRemoveAdmin_Click);
            // 
            // btnAddAdmin
            // 
            this.btnAddAdmin.Location = new System.Drawing.Point(1055, 348);
            this.btnAddAdmin.Name = "btnAddAdmin";
            this.btnAddAdmin.Size = new System.Drawing.Size(145, 35);
            this.btnAddAdmin.TabIndex = 19;
            this.btnAddAdmin.Text = "Add administrator";
            this.btnAddAdmin.UseVisualStyleBackColor = true;
            this.btnAddAdmin.Click += new System.EventHandler(this.btnAddAdmin_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1072, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 24);
            this.label5.TabIndex = 16;
            this.label5.Text = "Administrators";
            // 
            // btnUnblockVcard
            // 
            this.btnUnblockVcard.Location = new System.Drawing.Point(813, 430);
            this.btnUnblockVcard.Name = "btnUnblockVcard";
            this.btnUnblockVcard.Size = new System.Drawing.Size(145, 35);
            this.btnUnblockVcard.TabIndex = 24;
            this.btnUnblockVcard.Text = "Unblock Vcard";
            this.btnUnblockVcard.UseVisualStyleBackColor = true;
            this.btnUnblockVcard.Click += new System.EventHandler(this.btnUnblockVcard_Click_1);
            // 
            // btnBlockVcard
            // 
            this.btnBlockVcard.Location = new System.Drawing.Point(813, 389);
            this.btnBlockVcard.Name = "btnBlockVcard";
            this.btnBlockVcard.Size = new System.Drawing.Size(145, 35);
            this.btnBlockVcard.TabIndex = 23;
            this.btnBlockVcard.Text = "Block Vcard";
            this.btnBlockVcard.UseVisualStyleBackColor = true;
            this.btnBlockVcard.Click += new System.EventHandler(this.btnBlockVcard_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(831, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 24);
            this.label6.TabIndex = 21;
            this.label6.Text = "Vcard list";
            // 
            // btnExportXML
            // 
            this.btnExportXML.Location = new System.Drawing.Point(886, 567);
            this.btnExportXML.Name = "btnExportXML";
            this.btnExportXML.Size = new System.Drawing.Size(145, 35);
            this.btnExportXML.TabIndex = 28;
            this.btnExportXML.Text = "Export list as XML";
            this.btnExportXML.UseVisualStyleBackColor = true;
            this.btnExportXML.Click += new System.EventHandler(this.btnExportXML_Click);
            // 
            // btnExportXLSX
            // 
            this.btnExportXLSX.Location = new System.Drawing.Point(886, 610);
            this.btnExportXLSX.Name = "btnExportXLSX";
            this.btnExportXLSX.Size = new System.Drawing.Size(145, 35);
            this.btnExportXLSX.TabIndex = 27;
            this.btnExportXLSX.Text = "Export list as xlsx";
            this.btnExportXLSX.UseVisualStyleBackColor = true;
            this.btnExportXLSX.Click += new System.EventHandler(this.btnExportXLSX_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(574, 528);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 24);
            this.label7.TabIndex = 25;
            this.label7.Text = "Transaction Log";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(42, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 18);
            this.label8.TabIndex = 29;
            this.label8.Text = "Logged in";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Email: ";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(95, 54);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(77, 13);
            this.lblEmail.TabIndex = 31;
            this.lblEmail.Text = "mail@mail.com";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(33, 77);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Name:";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(98, 77);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(109, 13);
            this.lblUsername.TabIndex = 33;
            this.lblUsername.Text = "loggedUserUsername";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Location = new System.Drawing.Point(36, 94);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(171, 23);
            this.btnChangePassword.TabIndex = 34;
            this.btnChangePassword.Text = "Change my password";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // btnUnblockAdmin
            // 
            this.btnUnblockAdmin.Location = new System.Drawing.Point(1057, 471);
            this.btnUnblockAdmin.Name = "btnUnblockAdmin";
            this.btnUnblockAdmin.Size = new System.Drawing.Size(145, 35);
            this.btnUnblockAdmin.TabIndex = 36;
            this.btnUnblockAdmin.Text = "Unblock Administrador";
            this.btnUnblockAdmin.UseVisualStyleBackColor = true;
            this.btnUnblockAdmin.Click += new System.EventHandler(this.btnUnblockAdmin_Click);
            // 
            // btnBlockAdmin
            // 
            this.btnBlockAdmin.Location = new System.Drawing.Point(1057, 430);
            this.btnBlockAdmin.Name = "btnBlockAdmin";
            this.btnBlockAdmin.Size = new System.Drawing.Size(145, 35);
            this.btnBlockAdmin.TabIndex = 35;
            this.btnBlockAdmin.Text = "Block Administrator";
            this.btnBlockAdmin.UseVisualStyleBackColor = true;
            this.btnBlockAdmin.Click += new System.EventHandler(this.btnBlockAdmin_Click);
            // 
            // btnManageVcard
            // 
            this.btnManageVcard.Location = new System.Drawing.Point(813, 471);
            this.btnManageVcard.Name = "btnManageVcard";
            this.btnManageVcard.Size = new System.Drawing.Size(145, 35);
            this.btnManageVcard.TabIndex = 37;
            this.btnManageVcard.Text = "Manage Vcard";
            this.btnManageVcard.UseVisualStyleBackColor = true;
            this.btnManageVcard.Click += new System.EventHandler(this.btnManageVcard_Click);
            // 
            // btnCreateVcard
            // 
            this.btnCreateVcard.Location = new System.Drawing.Point(813, 348);
            this.btnCreateVcard.Name = "btnCreateVcard";
            this.btnCreateVcard.Size = new System.Drawing.Size(145, 35);
            this.btnCreateVcard.TabIndex = 41;
            this.btnCreateVcard.Text = "Create Vcard";
            this.btnCreateVcard.UseVisualStyleBackColor = true;
            this.btnCreateVcard.Click += new System.EventHandler(this.btnCreateVcard_Click);
            // 
            // lbTransactionLog
            // 
            this.lbTransactionLog.FormattingEnabled = true;
            this.lbTransactionLog.Location = new System.Drawing.Point(12, 651);
            this.lbTransactionLog.Name = "lbTransactionLog";
            this.lbTransactionLog.Size = new System.Drawing.Size(1253, 173);
            this.lbTransactionLog.TabIndex = 43;
            // 
            // lbFinancialEntities
            // 
            this.lbFinancialEntities.FormattingEnabled = true;
            this.lbFinancialEntities.Location = new System.Drawing.Point(12, 163);
            this.lbFinancialEntities.Name = "lbFinancialEntities";
            this.lbFinancialEntities.Size = new System.Drawing.Size(252, 95);
            this.lbFinancialEntities.TabIndex = 44;
            // 
            // lbAdminList
            // 
            this.lbAdminList.FormattingEnabled = true;
            this.lbAdminList.Location = new System.Drawing.Point(1005, 163);
            this.lbAdminList.Name = "lbAdminList";
            this.lbAdminList.Size = new System.Drawing.Size(252, 173);
            this.lbAdminList.TabIndex = 45;
            // 
            // lbDefaultCreditCat
            // 
            this.lbDefaultCreditCat.FormattingEnabled = true;
            this.lbDefaultCreditCat.Location = new System.Drawing.Point(538, 163);
            this.lbDefaultCreditCat.Name = "lbDefaultCreditCat";
            this.lbDefaultCreditCat.Size = new System.Drawing.Size(209, 173);
            this.lbDefaultCreditCat.TabIndex = 46;
            // 
            // lbDefaultDebitCat
            // 
            this.lbDefaultDebitCat.FormattingEnabled = true;
            this.lbDefaultDebitCat.Location = new System.Drawing.Point(303, 163);
            this.lbDefaultDebitCat.Name = "lbDefaultDebitCat";
            this.lbDefaultDebitCat.Size = new System.Drawing.Size(209, 173);
            this.lbDefaultDebitCat.TabIndex = 47;
            // 
            // lbVcardList
            // 
            this.lbVcardList.FormattingEnabled = true;
            this.lbVcardList.Location = new System.Drawing.Point(771, 163);
            this.lbVcardList.Name = "lbVcardList";
            this.lbVcardList.Size = new System.Drawing.Size(209, 173);
            this.lbVcardList.TabIndex = 48;
            // 
            // txtAddDebitCat
            // 
            this.txtAddDebitCat.Location = new System.Drawing.Point(339, 348);
            this.txtAddDebitCat.Name = "txtAddDebitCat";
            this.txtAddDebitCat.Size = new System.Drawing.Size(145, 20);
            this.txtAddDebitCat.TabIndex = 49;
            // 
            // txtAddCreditCat
            // 
            this.txtAddCreditCat.Location = new System.Drawing.Point(574, 348);
            this.txtAddCreditCat.Name = "txtAddCreditCat";
            this.txtAddCreditCat.Size = new System.Drawing.Size(145, 20);
            this.txtAddCreditCat.TabIndex = 50;
            // 
            // comboEntities
            // 
            this.comboEntities.FormattingEnabled = true;
            this.comboEntities.Location = new System.Drawing.Point(480, 555);
            this.comboEntities.Name = "comboEntities";
            this.comboEntities.Size = new System.Drawing.Size(121, 21);
            this.comboEntities.TabIndex = 53;
            // 
            // chkCredit
            // 
            this.chkCredit.AutoSize = true;
            this.chkCredit.Location = new System.Drawing.Point(694, 559);
            this.chkCredit.Name = "chkCredit";
            this.chkCredit.Size = new System.Drawing.Size(53, 17);
            this.chkCredit.TabIndex = 54;
            this.chkCredit.Text = "Credit";
            this.chkCredit.UseVisualStyleBackColor = true;
            // 
            // chkDebit
            // 
            this.chkDebit.AutoSize = true;
            this.chkDebit.Location = new System.Drawing.Point(753, 559);
            this.chkDebit.Name = "chkDebit";
            this.chkDebit.Size = new System.Drawing.Size(51, 17);
            this.chkDebit.TabIndex = 55;
            this.chkDebit.Text = "Debit";
            this.chkDebit.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(476, 583);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 20);
            this.label10.TabIndex = 56;
            this.label10.Text = "Between";
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(554, 582);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(100, 20);
            this.txtStartDate.TabIndex = 57;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(660, 583);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 20);
            this.label12.TabIndex = 58;
            this.label12.Text = "And";
            // 
            // txtEndDate
            // 
            this.txtEndDate.Location = new System.Drawing.Point(704, 582);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(100, 20);
            this.txtEndDate.TabIndex = 59;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(578, 619);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(141, 23);
            this.btnFilter.TabIndex = 60;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 847);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.txtEndDate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtStartDate);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.chkDebit);
            this.Controls.Add(this.chkCredit);
            this.Controls.Add(this.comboEntities);
            this.Controls.Add(this.txtAddCreditCat);
            this.Controls.Add(this.txtAddDebitCat);
            this.Controls.Add(this.lbVcardList);
            this.Controls.Add(this.lbDefaultDebitCat);
            this.Controls.Add(this.lbDefaultCreditCat);
            this.Controls.Add(this.lbAdminList);
            this.Controls.Add(this.lbFinancialEntities);
            this.Controls.Add(this.lbTransactionLog);
            this.Controls.Add(this.btnCreateVcard);
            this.Controls.Add(this.btnManageVcard);
            this.Controls.Add(this.btnUnblockAdmin);
            this.Controls.Add(this.btnBlockAdmin);
            this.Controls.Add(this.btnChangePassword);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnExportXML);
            this.Controls.Add(this.btnExportXLSX);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnUnblockVcard);
            this.Controls.Add(this.btnBlockVcard);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnRemoveAdmin);
            this.Controls.Add(this.btnAddAdmin);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnRemoveCreditCategory);
            this.Controls.Add(this.btnAddCreditCategory);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnRemoveDebitCategory);
            this.Controls.Add(this.btnAddDebitCategory);
            this.Controls.Add(this.btnRemoveFinancialEntity);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnEditFinancialEntity);
            this.Controls.Add(this.btnAddFinancialEntity);
            this.Controls.Add(this.btnUpdateStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administration Console";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUpdateStatus;
        private System.Windows.Forms.Button btnAddFinancialEntity;
        private System.Windows.Forms.Button btnEditFinancialEntity;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRemoveFinancialEntity;
        private System.Windows.Forms.Button btnRemoveDebitCategory;
        private System.Windows.Forms.Button btnAddDebitCategory;
        private System.Windows.Forms.Button btnRemoveCreditCategory;
        private System.Windows.Forms.Button btnAddCreditCategory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnRemoveAdmin;
        private System.Windows.Forms.Button btnAddAdmin;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnUnblockVcard;
        private System.Windows.Forms.Button btnBlockVcard;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnExportXML;
        private System.Windows.Forms.Button btnExportXLSX;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.Button btnUnblockAdmin;
        private System.Windows.Forms.Button btnBlockAdmin;
        private System.Windows.Forms.Button btnManageVcard;
        private System.Windows.Forms.Button btnCreateVcard;
        private System.Windows.Forms.ListBox lbTransactionLog;
        private System.Windows.Forms.ListBox lbFinancialEntities;
        private System.Windows.Forms.ListBox lbAdminList;
        private System.Windows.Forms.ListBox lbDefaultCreditCat;
        private System.Windows.Forms.ListBox lbDefaultDebitCat;
        private System.Windows.Forms.ListBox lbVcardList;
        private System.Windows.Forms.TextBox txtAddDebitCat;
        private System.Windows.Forms.TextBox txtAddCreditCat;
        private System.Windows.Forms.ComboBox comboEntities;
        private System.Windows.Forms.CheckBox chkCredit;
        private System.Windows.Forms.CheckBox chkDebit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtStartDate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtEndDate;
        private System.Windows.Forms.Button btnFilter;
    }
}

