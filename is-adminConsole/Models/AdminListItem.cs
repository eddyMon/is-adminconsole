﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    public class AdminListItem
    {
        public int Value { get; set; }
        public Admin Admin { get; set; }

        public AdminListItem(Admin admin, int value)
        {
            this.Value = value;
            this.Admin = admin;
        }

        public AdminListItem()
        {
        }


        public override string ToString()
        {
            return Admin.ToString();
        }
    }
}
