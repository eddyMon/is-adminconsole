﻿using System;

namespace is_adminConsole.Models
{
    public class Transaction
    {
        public int id { get; set; }

        public string date { get; set; }

        public double oldBalance { get; set; }

        public double newBalance { get; set; }


        public string originRef { get; set; }

        public string destRef { get; set; }


        public string paymentType { get; set; }


        public string paymentMethod { get; set; }


        public double value { get; set; }


        public int bankOrigin { get; set; }

        public int bankDest { get; set; }

        public string description { get; set; }

        //CATEGORIA A REVER SE PODE SER APENAS UMA STRING OU UM OBJECTO
        public string category { get; set; }

        public override string ToString()
        {
            return "[" + this.id + "]" + " - " + this.date + " - " + this.newBalance + " - " + this.oldBalance + " - " + this.originRef + " - " + this.destRef + " - " + this.paymentType + " - " + this.paymentMethod + " - " + this.bankOrigin + " - " + this.description + " - " + this.category;
        }
    }



}
