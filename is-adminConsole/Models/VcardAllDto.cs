﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    class VcardAllDto
    {
        public string phone_number { get; set; }

        public string bank { get; set; }
        public string name { get; set; }
        public double balance { get; set; }
        public double percentage { get; set; }
        public double maxDebit { get; set; }
        public Boolean isBlocked { get; set; }
    }
}
