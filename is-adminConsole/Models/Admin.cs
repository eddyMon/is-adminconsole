﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    public class Admin
    {
        public int Id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public Boolean isBlocked { get; set; }

        public override string ToString()
        {
            return "[" + this.Id + "] - " + this.name + " - " + (this.isBlocked ? "Blocked" : "Unblocked");
        }
    }
}
