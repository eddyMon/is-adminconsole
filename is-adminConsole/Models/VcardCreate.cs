﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace is_adminConsole.Models
{
    public class VcardCreate
    {
        [Required(ErrorMessage = "Phone number is required"), RegularExpression(@"[9][0-9]{8}\Z",
        ErrorMessage = "Phone format invalid, it should start with 9 and need to have 9 numbers")]
        public string phoneNumber { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string name { get; set; }
        [Required(ErrorMessage = "email is required")]
        public string email { get; set; }
        [Required(ErrorMessage = "IBAN is required")]
        public string IBAN { get; set; }
        [Required(ErrorMessage = "Code is required"), MinLength(4)]
        public string code { get; set; }
        [Required(ErrorMessage = "Password is required"), MinLength(8)]
        public string password { get; set; }

        public VcardCreate(string phoneNumber, string name, string email, string iBAN, string code, string password)
        {
            this.phoneNumber = phoneNumber;
            this.name = name;
            this.email = email;
            IBAN = iBAN;
            this.code = code;
            this.password = password;
        }
    }
}