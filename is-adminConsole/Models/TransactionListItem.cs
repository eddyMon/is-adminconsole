﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    public class TransactionListItem
    {
        public int Value { get; set; }
        public Transaction transaction { get; set; }


        public TransactionListItem()
        {
            
        }

        public TransactionListItem(Transaction transaction, int value)
        {
            this.transaction = transaction;
            this.Value = value;

        }
        
        public override string ToString()
        {
            return transaction.ToString();
        }

    }
}
