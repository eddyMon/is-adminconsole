﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    class Category
    {
        public int Id { get; set; }
        public string type { get; set; }
        public string name { get; set; }

        public override string ToString()
        {
            return "[" + this.Id + "] - " + this.name;
        }
    }
}
