﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    class CategoryListItem
    {
        public int Value { get; set; }
        public Category Category { get; set; }

        public CategoryListItem(Category Category, int value)
        {
            this.Value = value;
            this.Category = Category;
        }
        public CategoryListItem()
        {
        }
        public override string ToString()
        {
            return Category.ToString();
        }
    }

    
}
