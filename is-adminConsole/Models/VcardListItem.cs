﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    public class VcardListItem
    {
        public string phoneNumber { get; set; }
        public string name { get; set; }
        public string Bank { get; set; }
        public double balance { get; set; }
        public string blocked { get; set; }

        public VcardListItem()
        {
        }

        public VcardListItem(string phoneNumber, string name, string bank, double balance, string blocked)
        {
            this.phoneNumber = phoneNumber;
            this.name = name;
            Bank = bank;
            this.balance = balance;
            this.blocked = blocked;
        }

        public override string ToString()
        {
            return "[" + this.phoneNumber + "] - " + this.name + " - " + this.Bank + " - "
                + this.balance + " - " + this.blocked;
        }
    }
}
