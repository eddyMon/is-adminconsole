﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    public class BankListItem
    {
        public string Value { get; set; }
        public Bank Bank{ get; set; }
        public string status { get; set; }
        public BankListItem(Bank bank, string value,string status)
        {
            this.Value = value;
            this.Bank = bank;
            this.status = status;
        }

        public BankListItem()
        {
        }


        public override string ToString()
        {
            return Bank.ToString()+" - "+status;
        }
    }
}
