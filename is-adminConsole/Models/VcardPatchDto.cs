﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    class VcardPatchDto
    {
        [Range(0, 100.0)]
        public double? percentage { get; set; }

        [Range(0, Double.MaxValue)]
        public double? maxDebit { get; set; }

        public Boolean? isBlocked { get; set; }
    }
}
