﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_adminConsole.Models
{
    public class Bank
    {
        public string name { get; set; }
        public string bankCode { get; set; }
        public string endpoint { get; set; }

        public override string ToString()
        {
            return "[" + this.bankCode + "] - " + this.name;
        }
    }
}
