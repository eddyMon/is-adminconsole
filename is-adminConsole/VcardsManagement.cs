﻿using is_adminConsole.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace is_adminConsole
{
    public partial class VcardsManagement : Form
    {
        private Form1 form;
        private string phoneNumberStr;
        public VcardsManagement(Form1 form, string phone)
        {
            this.form = form;
            this.phoneNumberStr = phone;
            InitializeComponent();
            LoadFields();
            

        }

        private void LoadFields()
        {
            try
            {
                String GATEWAY_IP = Form1.GATEWAY_IP;
                var gatewayClient = new RestClient(GATEWAY_IP);
                var request = new RestRequest("api/vcards/" + this.phoneNumberStr, Method.GET);
                request.RequestFormat = DataFormat.Json;

                var qryResult = gatewayClient.Execute<VcardAllDto>(request);
                VcardAllDto item = qryResult.Data;

                //CHANGE TO CREATED
                if (qryResult.StatusCode == HttpStatusCode.OK)
                {
                    textBoxBalance.Text = item.balance.ToString();
                    textBoxBank.Text = item.bank;
                    textBoxName.Text = item.name;
                    textBoxPhone.Text = item.phone_number;
                    textBoxPercentage.Text = item.percentage.ToString();
                    textBoxMaxDebit.Text = item.maxDebit.ToString();
                    textBoxIsBlocked.Text = item.isBlocked.ToString();     

                }
                else
                {
                    MessageBox.Show(JsonConvert.DeserializeObject<ErrorMsgDto>(qryResult.Content).message);
                    Close();
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Something went wrong: " + ex.Message);
                Close();

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                String GATEWAY_IP = Form1.GATEWAY_IP;
                var gatewayClient = new RestClient(GATEWAY_IP);
                var request = new RestRequest("api/vcards/" + this.phoneNumberStr, Method.PATCH);
                request.RequestFormat = DataFormat.Json;
                

                request.AddJsonBody(new
                {
                    isBlocked = textBoxIsBlocked.Text,
                    percentage = textBoxPercentage.Text,
                    maxDebit = textBoxMaxDebit.Text
                });


                var qryResult = gatewayClient.Execute<VcardListItem>(request);
                VcardListItem item = qryResult.Data;
                //CHANGE TO CREATED
                if (qryResult.StatusCode == HttpStatusCode.OK)
                {

                    this.form.RefresItem(new VcardListItem(item.phoneNumber, item.name,
                        item.Bank, item.balance, item.blocked));
                    Close();

                }
                else
                {
                    try
                    {
                        MessageBox.Show(JsonConvert.DeserializeObject<ErrorMsgDto>(qryResult.Content).message);
                    }
                    catch (Exception)
                    {
                        //usually when Deserialize fails its because it an array with muktiple errors

                        MessageBox.Show(qryResult.Content);
                    }
                    
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Something went wrong: " + ex.Message);
            }




        }

        private void buttonCredit_Click(object sender, EventArgs e)
        {
            try
            {
                string value = textBoxCredit.Text;
                String GATEWAY_IP = Form1.GATEWAY_IP;
                var gatewayClient = new RestClient(GATEWAY_IP);
                var request = new RestRequest("api/transactions/admin/vcardcredit", Method.POST);
                request.RequestFormat = DataFormat.Json;
                    request.AddJsonBody(new
                    {
                        destRef = Form1.userName,
                        origin = textBoxPhone.Text,
                        method = "ADMIN_CREDIT",
                        value = value,
                    });


                var qryResult = gatewayClient.Execute<double>(request);
                double balance = qryResult.Data;
                if (qryResult.StatusCode == HttpStatusCode.Created)
                {

                    this.form.RefresItem(new VcardListItem(textBoxPhone.Text, textBoxName.Text,
                        textBoxBank.Text, balance, textBoxIsBlocked.Text));
                    MessageBox.Show("Credit to Vcard sucessful");

                }
                else
                {
                    try
                    {
                        MessageBox.Show(JsonConvert.DeserializeObject<ErrorMsgDto>(qryResult.Content).message);
                    }
                    catch (Exception)
                    {
                        //usually when Deserialize fails its because it is an array with muktiple errors

                        MessageBox.Show(qryResult.Content);
                    }

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Something went wrong: " + ex.Message);
            }

        }
    }
}
