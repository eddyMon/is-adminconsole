﻿using is_adminConsole.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace is_adminConsole
{
    public partial class addUser : Form
    {
        public Form1 form;
        public addUser(Form1 form)
        {
            InitializeComponent();
            this.form = form;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if(txtUserEmail.Text.Length == 0 || txtUserName.Text.Length == 0 || txtUserPassword.Text.Length == 0)
            {
                MessageBox.Show("You need to fill every field!");
                return;
            }

            //call API
            String GATEWAY_IP = Form1.GATEWAY_IP;
            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/admins", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new
            {
                name = txtUserName.Text,
                email = txtUserEmail.Text,
                password = txtUserPassword.Text
            }); ;

            var qryResult = gatewayClient.Execute<Admin>(request);

            if (qryResult.StatusCode == HttpStatusCode.Created)
            {
                this.form.UpdateAdminList(qryResult.Data);
                this.Close();
            }
            else
            {
                MessageBox.Show("There was an error creating the administrator -> "+ qryResult.ErrorMessage);
            }
            //if error send error message, else send success message and close dialog
        }
    }
}
