﻿using is_adminConsole.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace is_adminConsole
{
    public partial class addFinancialEntity : Form
    {
        public Form1 form;
        public addFinancialEntity(Form1 form)
        {
            InitializeComponent();
            this.form = form;
        }

        private void btnAddFinancial_Click(object sender, EventArgs e)
        {
            if (txtFinancialName.Text.Length == 0 || txtFinancialBankCode.Text.Length == 0 || txtFinancialEndpoint.Text.Length == 0)
            {
                MessageBox.Show("You need to fill every field!");
                return;
            }

            //call API
            String GATEWAY_IP = Form1.GATEWAY_IP;
            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/banks", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new
            {
                name = txtFinancialName.Text,
                bankCode = txtFinancialBankCode.Text,
                endpoint = txtFinancialEndpoint.Text
            }); ;

            var qryResult = gatewayClient.Execute<Bank>(request);
            //MessageBox.Show(qryResult.StatusCode.ToString());

            if (qryResult.StatusCode == HttpStatusCode.Created)
            {
                string status;
                try
                {
                    //get the bank status
                    var bankClient = new RestClient(txtFinancialEndpoint.Text);
                    var bankRequest = new RestRequest("api", Method.GET);

                    var bankQryResult = bankClient.Execute(bankRequest);
                    
                    if (bankQryResult.StatusCode == HttpStatusCode.OK)
                    {
                        status = "UP";
                    }
                    else
                    {
                        status = "DOWN";
                    }
                }
                catch (Exception)
                {
                    status = "DOWN";
                }
                

                //add bank to the list in the admin dashboard
                this.form.UpdateBankList(qryResult.Data, status);
                this.Close();
            }
            else
            {
                MessageBox.Show("There was an error creating the Financial Entity -> " + qryResult.ErrorMessage);
            }
        }
    }
}
