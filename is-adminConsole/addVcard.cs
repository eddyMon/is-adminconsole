﻿using is_adminConsole.Controllers;
using is_adminConsole.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace is_adminConsole
{
    public partial class addVcard : Form
    {
        public Form1 form;
        public addVcard(Form1 form)
        {
            InitializeComponent();
            this.form = form;
        }

        private void btnCreateVcard_Click(object sender, EventArgs e)
        {

            //FALTA PARTE DA AUTENTICAÇÃO


            string phoneNumber = txtVcardUsername.Text;
            string name = txtVcardName.Text;
            string email = txtVcardEmail.Text;
            string iban = txtVcardIBAN.Text;
            string code = txtVcardCode.Text;
            string password = txtVcardPassword.Text;

            VcardCreate vcard = new VcardCreate(phoneNumber, name, email, iban, code, password);

            //Verify DTO
            IEnumerable<ValidationResult> errors = Validations.getValidationErros(vcard);

            if (errors.Any())
            {

                MessageBox.Show(errors.First().ToString());
            }
            else
            {
                //SEND REQUEST TO CENTRALIZED API
                //Form1.GATEWAY_IP
                //call API
                try
                {
                    String GATEWAY_IP = Form1.GATEWAY_IP;
                    var gatewayClient = new RestClient(GATEWAY_IP);
                    var request = new RestRequest("api/vcards", Method.POST);
                    request.RequestFormat = DataFormat.Json;
                    request.AddBody(vcard);


                    var qryResult = gatewayClient.Execute<VcardListItem>(request);
                    VcardListItem item = qryResult.Data;
                    //CHANGE TO CREATED
                    if (qryResult.StatusCode == HttpStatusCode.Created)
                    {


                        this.form.UpdateVcardList(item);
                        this.Close();

                    }
                    else
                    {
                        MessageBox.Show(JsonConvert.DeserializeObject<ErrorMsgDto>(qryResult.Content).message);
                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show("Something went wrong: " + ex.Message);
                    Close();
                }
                




            }



        }
    }
}
