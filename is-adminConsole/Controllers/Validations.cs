﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace is_adminConsole.Controllers
{
    public class Validations
    {
        public static IEnumerable<ValidationResult> getValidationErros(object obj)
        {
            var validationResult = new List<ValidationResult>();
            var context = new ValidationContext(obj, null, null);
            Validator.TryValidateObject(obj, context, validationResult, true);
            return validationResult;
        }


    }
}