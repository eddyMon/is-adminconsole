﻿using is_adminConsole.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace is_adminConsole
{
    public partial class changePassword : Form
    {
        public changePassword()
        {
            InitializeComponent();
        }

        private void changePassword_Load(object sender, EventArgs e)
        {
            lblEmail.Text = Form1.userEmail;
            lblName.Text = Form1.userName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Right now it is hardcoded for the admin nº1, we need to change it later to fetch the authenticated user ID");
            //call API
            String GATEWAY_IP = Form1.GATEWAY_IP;
            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/admins/"+Form1.admin.Id+"/password", Method.PUT);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new
            {
                password = txtCurrPass.Text,
                newPassword = txtNewPass.Text
            }); ;

            var qryResult = gatewayClient.Execute<Admin>(request);

            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                this.Close();
                MessageBox.Show("Password updated successfuly");
            }
            else
            {
                MessageBox.Show("There was an error updating the password-> " + qryResult.ErrorMessage);
            }
        }
    }
}
