﻿
namespace is_adminConsole
{
    partial class editFinancialEntity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEditFinancial = new System.Windows.Forms.Button();
            this.txtFinancialEndpoint = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFinancialName = new System.Windows.Forms.Label();
            this.txtFinancialEntityName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnEditFinancial
            // 
            this.btnEditFinancial.Location = new System.Drawing.Point(158, 196);
            this.btnEditFinancial.Name = "btnEditFinancial";
            this.btnEditFinancial.Size = new System.Drawing.Size(75, 23);
            this.btnEditFinancial.TabIndex = 43;
            this.btnEditFinancial.Text = "Update";
            this.btnEditFinancial.UseVisualStyleBackColor = true;
            this.btnEditFinancial.Click += new System.EventHandler(this.btnEditFinancial_Click);
            // 
            // txtFinancialEndpoint
            // 
            this.txtFinancialEndpoint.Location = new System.Drawing.Point(122, 148);
            this.txtFinancialEndpoint.Name = "txtFinancialEndpoint";
            this.txtFinancialEndpoint.Size = new System.Drawing.Size(219, 20);
            this.txtFinancialEndpoint.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 20);
            this.label2.TabIndex = 40;
            this.label2.Text = "Endpoint:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(28, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 20);
            this.label9.TabIndex = 39;
            this.label9.Text = "Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 31);
            this.label1.TabIndex = 38;
            this.label1.Text = "Edit Financial Entity";
            // 
            // lblFinancialName
            // 
            this.lblFinancialName.AutoSize = true;
            this.lblFinancialName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinancialName.Location = new System.Drawing.Point(118, 53);
            this.lblFinancialName.Name = "lblFinancialName";
            this.lblFinancialName.Size = new System.Drawing.Size(138, 20);
            this.lblFinancialName.TabIndex = 44;
            this.lblFinancialName.Text = "PT66 - BankName";
            // 
            // txtFinancialEntityName
            // 
            this.txtFinancialEntityName.Location = new System.Drawing.Point(122, 98);
            this.txtFinancialEntityName.Name = "txtFinancialEntityName";
            this.txtFinancialEntityName.Size = new System.Drawing.Size(219, 20);
            this.txtFinancialEntityName.TabIndex = 45;
            // 
            // editFinancialEntity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 238);
            this.Controls.Add(this.txtFinancialEntityName);
            this.Controls.Add(this.lblFinancialName);
            this.Controls.Add(this.btnEditFinancial);
            this.Controls.Add(this.txtFinancialEndpoint);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.Name = "editFinancialEntity";
            this.Text = "editFinancialEntity";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEditFinancial;
        private System.Windows.Forms.TextBox txtFinancialEndpoint;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFinancialName;
        private System.Windows.Forms.TextBox txtFinancialEntityName;
    }
}