﻿
namespace is_adminConsole
{
    partial class addVcard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtVcardName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCreateVcard = new System.Windows.Forms.Button();
            this.txtVcardUsername = new System.Windows.Forms.TextBox();
            this.txtVcardEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVcardCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtVcardPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVcardIBAN = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtVcardName
            // 
            this.txtVcardName.Location = new System.Drawing.Point(184, 108);
            this.txtVcardName.Margin = new System.Windows.Forms.Padding(4);
            this.txtVcardName.Name = "txtVcardName";
            this.txtVcardName.Size = new System.Drawing.Size(288, 22);
            this.txtVcardName.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 108);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 25);
            this.label3.TabIndex = 52;
            this.label3.Text = "Name: ";
            // 
            // btnCreateVcard
            // 
            this.btnCreateVcard.Location = new System.Drawing.Point(120, 273);
            this.btnCreateVcard.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreateVcard.Name = "btnCreateVcard";
            this.btnCreateVcard.Size = new System.Drawing.Size(289, 28);
            this.btnCreateVcard.TabIndex = 51;
            this.btnCreateVcard.Text = "Create vcard";
            this.btnCreateVcard.UseVisualStyleBackColor = true;
            this.btnCreateVcard.Click += new System.EventHandler(this.btnCreateVcard_Click);
            // 
            // txtVcardUsername
            // 
            this.txtVcardUsername.Location = new System.Drawing.Point(184, 76);
            this.txtVcardUsername.Margin = new System.Windows.Forms.Padding(4);
            this.txtVcardUsername.Name = "txtVcardUsername";
            this.txtVcardUsername.Size = new System.Drawing.Size(288, 22);
            this.txtVcardUsername.TabIndex = 50;
            // 
            // txtVcardEmail
            // 
            this.txtVcardEmail.Location = new System.Drawing.Point(184, 140);
            this.txtVcardEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtVcardEmail.Name = "txtVcardEmail";
            this.txtVcardEmail.Size = new System.Drawing.Size(288, 22);
            this.txtVcardEmail.TabIndex = 49;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 73);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 25);
            this.label2.TabIndex = 48;
            this.label2.Text = "Phone Number:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(23, 137);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 25);
            this.label9.TabIndex = 47;
            this.label9.Text = "Email: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(159, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 46);
            this.label1.TabIndex = 46;
            this.label1.Text = "Add vcard";
            // 
            // txtVcardCode
            // 
            this.txtVcardCode.Location = new System.Drawing.Point(184, 197);
            this.txtVcardCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtVcardCode.Name = "txtVcardCode";
            this.txtVcardCode.PasswordChar = '*';
            this.txtVcardCode.Size = new System.Drawing.Size(288, 22);
            this.txtVcardCode.TabIndex = 55;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 197);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 25);
            this.label4.TabIndex = 54;
            this.label4.Text = "Code:";
            // 
            // txtVcardPassword
            // 
            this.txtVcardPassword.Location = new System.Drawing.Point(184, 229);
            this.txtVcardPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtVcardPassword.Name = "txtVcardPassword";
            this.txtVcardPassword.PasswordChar = '*';
            this.txtVcardPassword.Size = new System.Drawing.Size(288, 22);
            this.txtVcardPassword.TabIndex = 57;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 229);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 25);
            this.label5.TabIndex = 56;
            this.label5.Text = "Password:";
            // 
            // txtVcardIBAN
            // 
            this.txtVcardIBAN.Location = new System.Drawing.Point(184, 167);
            this.txtVcardIBAN.Margin = new System.Windows.Forms.Padding(4);
            this.txtVcardIBAN.Name = "txtVcardIBAN";
            this.txtVcardIBAN.Size = new System.Drawing.Size(288, 22);
            this.txtVcardIBAN.TabIndex = 59;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 167);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 25);
            this.label6.TabIndex = 58;
            this.label6.Text = "IBAN:";
            // 
            // addVcard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 332);
            this.Controls.Add(this.txtVcardIBAN);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtVcardPassword);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtVcardCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtVcardName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCreateVcard);
            this.Controls.Add(this.txtVcardUsername);
            this.Controls.Add(this.txtVcardEmail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "addVcard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "addVcard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtVcardName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCreateVcard;
        private System.Windows.Forms.TextBox txtVcardUsername;
        private System.Windows.Forms.TextBox txtVcardEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVcardCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtVcardPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVcardIBAN;
        private System.Windows.Forms.Label label6;
    }
}