﻿using is_adminConsole.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace is_adminConsole
{
    public partial class Form1 : Form
    {
        MqttClient mClient = new MqttClient("127.0.0.1");
        //Specify which topics we are interested in
        string[] mStrTopicsInfo = { "transactions", "test" };
        public static string GATEWAY_IP = "https://localhost:44384/";
        public static Admin admin = null;

        public static string userEmail = "";
        public static string userName = "ADMIn_test";
        //public static string financialEntityName = "";
        public Form1(Admin admin)
        {
            InitializeComponent();
            Form1.admin = admin;
            Form1.userEmail = admin.email;
            Form1.userName = admin.email;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //GATEWAY CONN
            var gatewayClient = new RestClient(GATEWAY_IP);

            lblEmail.Text = loginForm.loginEmail;
            lblUsername.Text = loginForm.username;

            userEmail = loginForm.loginEmail;
            userName = loginForm.username;
            //fetch username via email

            //First line on the transaction log
            //ID - DATE - ORIGIN_REF - TYPE - METHOD - DEST_REF - CATEGORY - DESCRIPTION
            //lbTransactionLog.Items.Add("ID\tDate\t\tOrigin Reference\tOrigin Bank\tType\t\tMethod\tDestination\tAmount\tOld Balance\tNew Balance\tCategory\t\tDescription");

            //lbTransactionLog.Items.Add("54654\t2021-11-01\t999654321\tBank 1\t\tDebit\t\tVCARD\t915532154\t500\t1500\t\t2000\t\tFuel\t\tHappy birthday");

            //fetch all transactions for the DB
            try
            {
                var request = new RestRequest("api/transactions/admin", Method.GET);
                request.AddHeader("Content-Type", "application/json");
                var transactionListQueryResult = gatewayClient.Execute<List<Transaction>>(request).Data;
               
                foreach (var transaction in transactionListQueryResult)
                {
                    //1st arg = display text; 2nd arg = list item value
                    lbTransactionLog.Items.Add(new TransactionListItem() { transaction = transaction, Value = transaction.id });
                }
                lbTransactionLog.DisplayMember = "Text";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error loading the transaction list");
            }
            //message broker connection
            mClient.Connect(Guid.NewGuid().ToString());
            if (!mClient.IsConnected)
            {
                MessageBox.Show("Error connecting to message broker...");
                return;
            }


            //New Msg Arrived
            mClient.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            //QoS
            byte[] qosLevels = { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE };
            //Subscribe to the transactions topic
            mClient.Subscribe(mStrTopicsInfo, qosLevels);


            //fetch platform max debit limit

            //fetch all admins
            try
            {
                var request = new RestRequest("api/admins", Method.GET);
                var adminListQueryResult = gatewayClient.Execute<List<Admin>>(request).Data;
                foreach (var admin in adminListQueryResult)
                {
                    //1st arg = display text; 2nd arg = list item value
                    lbAdminList.Items.Add(new AdminListItem() { Admin = admin, Value = admin.Id });
                }
                lbAdminList.DisplayMember = "Text";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error loading the administrator list");
            }
            //lbAdminList.DataSource = adminListQueryResult;

            //fetch all vcards
            try
            {
                var request = new RestRequest("api/vcards", Method.GET);
                List<VcardListItem> vcardListQueryResult = gatewayClient.Execute<List<VcardListItem>>(request).Data;
                foreach (var vcard in vcardListQueryResult)
                {

                    lbVcardList.Items.Add(vcard);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("There was an error loading the credit category list");
            }

            //fetch all credit categories
            try
            {
                var request = new RestRequest("api/categories/credit", Method.GET);
                var creditCatListQueryResult = gatewayClient.Execute<List<Category>>(request).Data;
                foreach (var category in creditCatListQueryResult)
                {
                    //1st arg = display text; 2nd arg = list item value
                    lbDefaultCreditCat.Items.Add(new CategoryListItem() { Category = category, Value = category.Id });
                }
                lbDefaultCreditCat.DisplayMember = "Text";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error loading the credit category list");
            }

            //fetch all debit categories
            try
            {
                var request = new RestRequest("api/categories/debit", Method.GET);
                var debitCatListQueryResult = gatewayClient.Execute<List<Category>>(request).Data;
                foreach (var category in debitCatListQueryResult)
                {
                    //1st arg = display text; 2nd arg = list item value
                    lbDefaultDebitCat.Items.Add(new CategoryListItem() { Category = category, Value = category.Id });
                }
                lbDefaultDebitCat.DisplayMember = "Text";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error loading the debit category list");
            }
            //fetch all financial entities
            try
            {
                var request = new RestRequest("api/banks", Method.GET);
                var bankListQueryResult = gatewayClient.Execute<List<Bank>>(request).Data;
                foreach (var bank in bankListQueryResult)
                {
                    string status;
                    try
                    {
                        //check if the bank is up or down by pinging its endpoint
                        var bankItemRequest = new RestRequest("api", Method.GET);
                        var bankItemClient = new RestClient(bank.endpoint);
                        var bankItemQueryStatusCode = bankItemClient.Execute(bankItemRequest).StatusCode;
                        status = bankItemQueryStatusCode == HttpStatusCode.OK ? "UP" : "DOWN";
                    }
                    catch (Exception)
                    {
                        status = "DOWN";
                    }
                    

                    //MessageBox.Show(bank.endpoint);
                    lbFinancialEntities.Items.Add(new BankListItem() { Bank = bank, Value = bank.endpoint,status = status });
                }
                lbFinancialEntities.DisplayMember = "Text";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error loading the financial entities list");
            }

            //populate entities combo box
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            var adminConsole = new changePassword();
            adminConsole.Show();
        }

        void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            //MessageBox.Show("Received = " + Encoding.UTF8.GetString(e.Message) + " on topic " +e.Topic);

            if (lbTransactionLog.InvokeRequired)
            {
                //if the incoming transactions matches all the current transaction log filters we add it to the list.
                //otherwise
                //we dont
                lbTransactionLog.Invoke(new MethodInvoker(delegate { lbTransactionLog.Items.Add(Encoding.UTF8.GetString(e.Message)); }));
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (mClient.IsConnected)
            {
                mClient.Unsubscribe(mStrTopicsInfo); //Put this in a button to see notif!
                mClient.Disconnect(); //Free process and process's resources
            }
        }

        private void btnAddFinancialEntity_Click(object sender, EventArgs e)
        {
            var financialEntityAdd = new addFinancialEntity(this);
            financialEntityAdd.Show();
        }

        private void btnEditFinancialEntity_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbFinancialEntities.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You need to select a Financial entity first");
            }
            else
            {
                //financialEntityName = lbFinancialEntities.GetItemText(lbFinancialEntities.SelectedItem);
                BankListItem selectedElement = (lbFinancialEntities.SelectedItem as BankListItem);
                var financialEntityEdit = new editFinancialEntity(this, selectedIndex, selectedElement);
                financialEntityEdit.Show();
            }
        }

        private void btnRemoveFinancialEntity_Click(object sender, EventArgs e)
        {

            int selectedIndex = lbFinancialEntities.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You need to select a Financial entity first");
                return;
            }
            else
            {

                BankListItem selectedElement = (lbFinancialEntities.SelectedItem as BankListItem);

                string bankCode = selectedElement.Bank.bankCode;

                var gatewayClient = new RestClient(GATEWAY_IP);
                var request = new RestRequest("api/banks/" + bankCode, Method.DELETE);
                var qryResult = gatewayClient.Execute(request);
                if (qryResult.StatusCode == HttpStatusCode.OK)
                {
                    lbFinancialEntities.Items.RemoveAt(selectedIndex);
                    MessageBox.Show("Financial Entity successfully deleted");
                }
                else
                {
                    MessageBox.Show("There was an error removing the selected administrator");
                }
            }

        }

        private void btnAddDebitCategory_Click(object sender, EventArgs e)
        {
            if (txtAddDebitCat.TextLength == 0)
            {
                MessageBox.Show("Please insert the name of the category you want to add");
                return;
            }

            //call API
            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/categories", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new
            {
                name = txtAddDebitCat.Text,
                type = "D"
            }); ;

            var qryResult = gatewayClient.Execute<Category>(request);

            if (qryResult.StatusCode == HttpStatusCode.Created)
            {
                //MessageBox.Show("New category -> " + qryResult.Content);
                Category cat = qryResult.Data;
                lbDefaultDebitCat.Items.Add(new CategoryListItem() { Category = cat, Value = cat.Id });
            }
            else
            {
                MessageBox.Show("There was an error creating the debit category -> " + qryResult.ErrorMessage);
            }
        }

        private void btnAddCreditCategory_Click(object sender, EventArgs e)
        {
            if (txtAddCreditCat.TextLength == 0)
            {
                MessageBox.Show("Please insert the name of the category you want to add");
                return;
            }

            //call API
            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/categories", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new
            {
                name = txtAddCreditCat.Text,
                type = "C"
            }); ;

            var qryResult = gatewayClient.Execute<Category>(request);

            if (qryResult.StatusCode == HttpStatusCode.Created)
            {
                //MessageBox.Show("New category -> " + qryResult.Content);
                Category cat = qryResult.Data;
                lbDefaultCreditCat.Items.Add(new CategoryListItem() { Category = cat, Value = cat.Id });
            }
            else
            {
                MessageBox.Show("There was an error creating the credit category -> " + qryResult.ErrorMessage);
            }
        }

        private void btnRemoveDebitCategory_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbDefaultDebitCat.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You need to select a debit category first!");
                return;
            }

            CategoryListItem selectedElement = (lbDefaultDebitCat.SelectedItem as CategoryListItem);

            int id = selectedElement.Value;

            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/categories/" + id, Method.DELETE);

            var qryResult = gatewayClient.Execute(request);

            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                //update the listbox text after blocking
                lbDefaultDebitCat.Items.RemoveAt(selectedIndex);
                MessageBox.Show("Debit category successfully deleted");
            }
            else
            {
                MessageBox.Show("There was an error removing the selected debit category");
            }
        }

        private void btnRemoveCreditCategory_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbDefaultCreditCat.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You need to select a credit category first!");
                return;
            }

            CategoryListItem selectedElement = (lbDefaultCreditCat.SelectedItem as CategoryListItem);

            int id = selectedElement.Value;

            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/categories/" + id, Method.DELETE);

            var qryResult = gatewayClient.Execute(request);

            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                //update the listbox text after blocking
                lbDefaultCreditCat.Items.RemoveAt(selectedIndex);
                MessageBox.Show("Credit category successfully deleted");
            }
            else
            {
                MessageBox.Show("There was an error removing the selected credit category");
            }
        }

        private void btnAddAdmin_Click(object sender, EventArgs e)
        {
            var addUserForm = new addUser(this);
            addUserForm.Show();
        }

        private void btnCreateVcard_Click(object sender, EventArgs e)
        {
            var addVcardForm = new addVcard(this);
            addVcardForm.Show();
        }

        private void btnBlockAdmin_Click(object sender, EventArgs e)
        {
            lbAdminList.Refresh();
            int selectedIndex = lbAdminList.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You need to select an administrator first!");
                return;
            }

            AdminListItem selectedElement = (lbAdminList.SelectedItem as AdminListItem);

            int id = selectedElement.Value;

            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/admins/" + id, Method.PATCH);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { isBlocked = true });

            var qryResult = gatewayClient.Execute(request);

            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                selectedElement.Admin.isBlocked = true;
                //update the listbox text after blocking
                lbAdminList.Items.RemoveAt(selectedIndex);
                lbAdminList.Items.Insert(selectedIndex, selectedElement);
                MessageBox.Show("Administrator successfully blocked");
            }
            else
            {
                MessageBox.Show("There was an error blocking the selected administrator");
            }
        }

        private void btnUnblockAdmin_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbAdminList.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You need to select an administrator first!");
                return;
            }

            AdminListItem selectedElement = (lbAdminList.SelectedItem as AdminListItem);

            int id = selectedElement.Value;

            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/admins/" + id, Method.PATCH);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { isBlocked = false });

            var qryResult = gatewayClient.Execute(request);

            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                selectedElement.Admin.isBlocked = false;
                //update the listbox text after blocking
                lbAdminList.Items.RemoveAt(selectedIndex);
                lbAdminList.Items.Insert(selectedIndex, selectedElement);
                MessageBox.Show("Administrator successfully unblocked");
            }
            else
            {
                MessageBox.Show("There was an error unblocking the selected administrator");
            }
        }
        public void RefreshBankList(Bank bank,int index, string oldStatus)
        {
            lbFinancialEntities.Items.RemoveAt(index);
            lbFinancialEntities.Items.Insert(index, new BankListItem() {Bank = bank, Value = bank.endpoint, status = oldStatus });

            MessageBox.Show("Financial Entity edited successfully!");
        }

        public void UpdateBankList(Bank bank,string status)
        {
            lbFinancialEntities.Items.Add(new BankListItem() { Bank = bank, Value = bank.endpoint, status = status});
            MessageBox.Show("Financial Entity created successfully!");
        }
        public void UpdateAdminList(Admin admin)
        {
            lbAdminList.Items.Add(new AdminListItem() { Admin = admin, Value = admin.Id });
            MessageBox.Show("Administrator created successfully!");
        }
        public void UpdateVcardList(VcardListItem vcard)
        {
            lbVcardList.Items.Add(vcard);
            MessageBox.Show("Vcard created successfully!");
        }
        public void RefresItem(VcardListItem vcard)
        {
            lbVcardList.Items.RemoveAt(lbVcardList.SelectedIndex);
            lbVcardList.Items.Add(vcard);
            MessageBox.Show("Vcard Updated successfully!");
        }

        private void btnRemoveAdmin_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbAdminList.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You need to select an administrator first!");
                return;
            }

            AdminListItem selectedElement = (lbAdminList.SelectedItem as AdminListItem);

            int id = selectedElement.Value;

            var gatewayClient = new RestClient(GATEWAY_IP);
            var request = new RestRequest("api/admins/" + id, Method.DELETE);
            request.RequestFormat = DataFormat.Json;

            var qryResult = gatewayClient.Execute(request);

            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                selectedElement.Admin.isBlocked = false;
                //update the listbox text after blocking
                lbAdminList.Items.RemoveAt(selectedIndex);
                MessageBox.Show("Administrator successfully deleted");
            }
            else
            {
                MessageBox.Show("There was an error removing the selected administrator");
            }
        }

        private void btnBlockVcard_Click(object sender, EventArgs e)
        {


            try
            {
                string phoneNumber = lbVcardList.SelectedItem.ToString().Substring(1, 9);
                String GATEWAY_IP = Form1.GATEWAY_IP;
                var gatewayClient = new RestClient(GATEWAY_IP);
                var request = new RestRequest("api/vcards/" + phoneNumber, Method.PATCH);
                request.RequestFormat = DataFormat.Json;
                request.AddJsonBody(new
                {
                    isBlocked = true
                });


                var qryResult = gatewayClient.Execute<VcardListItem>(request);
                VcardListItem item = qryResult.Data;
                //CHANGE TO CREATED
                if (qryResult.StatusCode == HttpStatusCode.OK)
                {


                    this.lbVcardList.Items.RemoveAt(lbVcardList.SelectedIndex);
                    this.lbVcardList.Items.Add(item);
                    MessageBox.Show("Vcard blocked");


                }
                else
                {
                    MessageBox.Show(JsonConvert.DeserializeObject<ErrorMsgDto>(qryResult.Content).message);
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Something went wrong: " + ex.Message);
            }
        }

        private void btnUnblockVcard_Click_1(object sender, EventArgs e)
        {
            try
            {
                string phoneNumber = lbVcardList.SelectedItem.ToString().Substring(1, 9);
                String GATEWAY_IP = Form1.GATEWAY_IP;
                var gatewayClient = new RestClient(GATEWAY_IP);
                var request = new RestRequest("api/vcards/" + phoneNumber, Method.PATCH);
                request.RequestFormat = DataFormat.Json;
                request.AddJsonBody(new
                {
                    isBlocked = false
                });


                var qryResult = gatewayClient.Execute<VcardListItem>(request);
                VcardListItem item = qryResult.Data;
                //CHANGE TO CREATED
                if (qryResult.StatusCode == HttpStatusCode.OK)
                {


                    this.lbVcardList.Items.RemoveAt(lbVcardList.SelectedIndex);
                    this.lbVcardList.Items.Add(item);
                    MessageBox.Show("Vcard unblocked");


                }
                else
                {

                    try
                    {
                        MessageBox.Show(JsonConvert.DeserializeObject<ErrorMsgDto>(qryResult.Content).message);
                    }
                    catch (Exception)
                    {
                        //usually when Deserialize fails its because it an array with muktiple errors

                        MessageBox.Show(qryResult.Content);
                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Something went wrong: " + ex.Message);

            }

        }

        private void btnManageVcard_Click(object sender, EventArgs e)
        {
            try
            {
                string phoneNumber = lbVcardList.SelectedItem.ToString().Substring(1, 9);
                var vcardManagement = new VcardsManagement(this, phoneNumber);
                vcardManagement.Show();
            }
            catch (Exception)
            {

                MessageBox.Show("Somethin went wrong");
            }


        }

        private void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbFinancialEntities.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You need to select a Financial Entity first!");
                return;
            }

            BankListItem selectedElement = (lbFinancialEntities.SelectedItem as BankListItem);

            string endpoint = selectedElement.Bank.endpoint;

            var bankClient = new RestClient(endpoint);
            var request = new RestRequest("api", Method.GET);

            var qryResult = bankClient.Execute(request);

            if (qryResult.StatusCode == HttpStatusCode.OK)
            {
                selectedElement.status = "UP";
            }
            else
            {
                selectedElement.status = "DOWN";
            }
            //update the listbox text after updating the status
            lbFinancialEntities.Items.RemoveAt(selectedIndex);
            lbFinancialEntities.Items.Insert(selectedIndex, selectedElement);
            MessageBox.Show("Status for the selected Financial Entity updated");
        }

        private void btnExportXML_Click(object sender, EventArgs e)
        {
            try
            {
                string[] xmlArr = new string[1];
                xmlArr[0] = CreateXML();
                //string path = @"C:\Users\eduar\Documents\TransactionLog.xml";
                string path = AppDomain.CurrentDomain.BaseDirectory+@"\" + "TransactionLog.xml";
                //MessageBox.Show(path);
                File.WriteAllLines(path, xmlArr);
                MessageBox.Show("Succefully exported the transaction list items to an XML file");
                
            }
            catch (Exception exception)
            {
                MessageBox.Show("There was an error exporting the list to XML ->"+ exception.Message);
            }
        }

        public string CreateXML()
        {
            XmlDocument doc = new XmlDocument();
            //XML Declaration + append to XML document
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);
            //create root element
            XmlElement root = doc.CreateElement("transactionlog");
            doc.AppendChild(root);

            //fetch all transactions from the transaction listbox
            foreach(TransactionListItem listBoxItem in lbTransactionLog.Items)
            {
                Transaction currTransaction = listBoxItem.transaction;
                //create transaction xml element
                root.AppendChild(createTransaction(doc, currTransaction.id,currTransaction.date, currTransaction.oldBalance, 
                    currTransaction.newBalance, currTransaction.originRef, currTransaction.destRef, currTransaction.paymentType,
                    currTransaction.paymentMethod, currTransaction.value, currTransaction.bankOrigin, currTransaction.bankDest,
                    currTransaction.description, currTransaction.category));
            }
            //root.AppendChild(createTransaction(doc, "WEB", "Learning XML", "Erik t. Ray", "2003", "39.95"));

            doc.Save(@"sample.xml");
            return doc.OuterXml;
        }

        public XmlElement createTransaction(
            XmlDocument doc, int id, string date, double oldBalance, 
            double newBalance, string originRef, string destRef, 
            string paymentType, string paymentMethod, double value, 
            int bankOrigin, int bankDest, string description, string category)
        {
            XmlElement transaction = doc.CreateElement("transaction");

            XmlElement idXML = doc.CreateElement("id");
            XmlElement dateXML = doc.CreateElement("date");
            XmlElement oldBalanceXML = doc.CreateElement("oldBalance");
            XmlElement newBalanceXML = doc.CreateElement("newBalance");
            XmlElement originRefXML = doc.CreateElement("originRef");
            XmlElement destRefXML = doc.CreateElement("destRef");
            XmlElement paymentTypeXML = doc.CreateElement("paymentType");
            XmlElement paymentMethodXML = doc.CreateElement("paymentMethod");
            XmlElement valueXML = doc.CreateElement("value");
            XmlElement bankOriginXML = doc.CreateElement("bankOrigin");
            XmlElement bankDestXML = doc.CreateElement("bankDest");
            XmlElement descriptionXML = doc.CreateElement("description");
            XmlElement categoryXML = doc.CreateElement("category");

            idXML.InnerText = id.ToString();
            dateXML.InnerText = date;
            oldBalanceXML.InnerText = oldBalance.ToString();
            newBalanceXML.InnerText = newBalance.ToString();
            originRefXML.InnerText = oldBalance.ToString();
            destRefXML.InnerText = destRef.ToString();
            paymentTypeXML.InnerText = paymentType.ToString();
            paymentMethodXML.InnerText = paymentMethod.ToString();
            valueXML.InnerText = value.ToString();
            bankDestXML.InnerText = bankDest.ToString();
            bankOriginXML.InnerText = bankOrigin.ToString();
            descriptionXML.InnerText = description == null ? "" : description;
            categoryXML.InnerText = category == null ? "" : category;

            transaction.AppendChild(idXML);
            transaction.AppendChild(dateXML);
            transaction.AppendChild(oldBalanceXML);
            transaction.AppendChild(newBalanceXML);
            transaction.AppendChild(originRefXML);
            transaction.AppendChild(destRefXML);
            transaction.AppendChild(paymentTypeXML);
            transaction.AppendChild(paymentMethodXML);
            transaction.AppendChild(valueXML);
            transaction.AppendChild(bankOriginXML);
            transaction.AppendChild(bankDestXML);
            if(category != null && category == "")
            {
                transaction.AppendChild(categoryXML);
            }
            if (description != null && description == "")
            {
                transaction.AppendChild(descriptionXML);
            }

            return transaction;
        }

        private void btnExportXLSX_Click(object sender, EventArgs e)
        {
            try
            {
                
                //string path = @"C:\Users\eduar\Documents\TransactionLog.xml";
                string path = AppDomain.CurrentDomain.BaseDirectory + @"\" + "TransactionLog.xlsx";
                //MessageBox.Show(path);
                //File.WriteAllLines(path, xmlArr);
                //ExcelHandler.WriteToExcelFile(path);

                ExcelHandler.CreateNewExcelFile(path);
                int listLength = lbTransactionLog.Items.Count;
                string[,] transMatrix = new string[listLength, 50];
                for (int i = 0; i < lbTransactionLog.Items.Count; i++)
                {
                    TransactionListItem listBoxItem = (TransactionListItem)lbTransactionLog.Items[i];
                    Transaction currTransaction = listBoxItem.transaction;
                    //create transaction xml element
                    transMatrix[i, 0] = currTransaction.id.ToString();
                    transMatrix[i, 1] = currTransaction.date.ToString();
                    transMatrix[i, 2] = currTransaction.oldBalance.ToString();
                    transMatrix[i, 3] = currTransaction.newBalance.ToString();
                    transMatrix[i, 4] = currTransaction.originRef.ToString();
                    transMatrix[i, 5] = currTransaction.destRef.ToString();
                    transMatrix[i, 6] = currTransaction.paymentType.ToString();
                    transMatrix[i, 7] = currTransaction.paymentMethod.ToString();
                    transMatrix[i, 8] = currTransaction.value.ToString();
                    transMatrix[i, 9] = currTransaction.bankOrigin.ToString();
                    transMatrix[i, 10] = currTransaction.bankDest.ToString();
                    transMatrix[i, 11] = currTransaction.description.ToString();
                    transMatrix[i, 12] = currTransaction.category.ToString();
                   
                    ExcelHandler.WriteToExcelFile(path, transMatrix);
                }

                MessageBox.Show("Succefully exported the transaction list items to an Excel file");

            }
            catch (Exception exception)
            {
                MessageBox.Show("There was an error exporting the list to Excel ->" + exception.Message);
            }
        }
    }
}
